/*
   Метод для отображения на экране стартовой надписи
*/
void start_LCD() {
  LD.printString_6x8("Для начала", 30, 1);
  LD.printString_6x8("заполни систему", 15, 2);
  LD.printString_6x8("продуктом", 35, 3);
  LD.printString_6x8("нажатием кнопки", 15, 4);
  LD.printString_6x8("ручного дозирования", 5, 5);
}

/*
   Метод для отображения на экране заряды батареи
*/
void bat_LCD() {
  LD.printString_6x8("Bat", 80, 1);
  LD.printString_6x8("   ", 105, 1);
  LD.printNumber((long)bat_vol, 105, 1);
  LD.printString_6x8("%", 120, 1);
}

/*
   Метод для отображения на экране заряды батареи
*/
void res_LCD() {
  LD.clearDisplay();
}

void stopPumpLCD() {
  LD.printString_6x8("                      ", 5, 2);
  LD.printString_6x8(" Поставь хоть одну    ", 5, 3);
  LD.printString_6x8(" рюмку, или будем     ", 5, 4);
  LD.printString_6x8(" наливать в ладошки?   ", 5, 5);
  LD.printString_6x8("                       ", 5, 6);
  LD.printString_6x8("                       ", 5, 7);
}

void stopVolLCD() {
  LD.printString_6x8("                       ", 10, 2);
  LD.printString_6x8(" Объем равен 0.        ", 10, 3);
  LD.printString_6x8(" Ты выпить хочешь?     ", 10, 4);
  LD.printString_6x8(" или как?              ", 10, 5);
  LD.printString_6x8("                       ", 10, 6);
  LD.printString_6x8("                       ", 10, 7);
}

void runPumpLCD() {
  LD.printString_6x8("     Наливаю по       ", 10, 2);
  LD.printString_6x8("                      ", 10, 3);
  LD.printString_6x8("мл.", 70, 3);
  LD.printNumber((long)vol, 50, 3);
  LD.printString_6x8("                      ", 10, 4);
  LD.printString_6x8("                       ", 10, 5);
  LD.printString_6x8("                       ", 10, 6);
  LD.printString_6x8("                       ", 10, 7);
}

void autoMode_LCD() {
  LD.printString_6x8("     Авто режим       ", 5, 0);
  LD.printString_6x8("ставь свой тазик а я  ", 5, 1);
  LD.printString_6x8("налью тебе            ", 5, 2);
  LD.printString_6x8("мл.", 90, 2);
  LD.printNumber((long)vol, 70, 2);

  LD.printString_6x8("крути крутилку чтобы  ", 5, 3);
  LD.printString_6x8("изменить обьем        ", 5, 4);
  LD.printString_6x8("Жми L+R чтобы выйти   ", 5, 5);
}

void autoModeExit_LCD(int c, int v) {
  LD.printString_6x8("Выход из авто режима  ", 5, 1);
  LD.printString_6x8("всего было налито", 5, 2);

  LD.printString_6x8("           ", 5, 3);
  LD.printString_6x8("мл.", 70, 3);
  LD.printNumber((long)v, 50, 3);

  LD.printString_6x8("в ", 5, 4);
  LD.printString_6x8("    ", 10, 4);
  LD.printNumber((long)c, 50, 4);
  LD.printString_6x8("тазиков", 70, 4);

  LD.printString_6x8("                      ", 5, 5);
  LD.printString_6x8("                      ", 5, 7);
  LD.printString_6x8("                      ", 5, 6);
}

void resPumpLCD(int count) {
  LD.printString_6x8(" Было налито по       ", 10, 2);
  LD.printString_6x8("                      ", 10, 3);
  LD.printString_6x8("                      ", 10, 4);
  LD.printString_6x8("мл.", 40, 3);
  LD.printNumber((long)vol, 20, 3);
  LD.printString_6x8(" тазиков    ", 40, 4);
  LD.printNumber((long)count, 20, 4);
  LD.printString_6x8("жду следующей партии", 5, 5);
  LD.printString_6x8("                       ", 10, 6);
  LD.printString_6x8("                       ", 10, 7);
}

void posCal_LCD(int index) {
  LD.printString_6x8("Выбери позицию для    ", 0, 0);
  LD.printString_6x8("настройки от 1 до 5   ", 0, 1);
  LD.printString_6x8("кнопками L или R      ", 0, 2);
  LD.printString_6x8("Выбор кнопка S        ", 0, 3);
  LD.printString_6x8("Текущая позиция: ", 0, 4);
  LD.printString_6x8("     ", 90, 4);
  LD.printNumber((long)index, 95, 4);
  LD.printString_6x8("                      ", 0, 5);
   LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}

void posCalSelect_LCD(int p) {
  LD.printString_6x8("Для настр. положения  ", 0, 0);
  LD.printString_6x8("жми кнопки L или R    ", 0, 1);
  LD.printString_6x8("проверка позиции по   ", 0, 2);
  LD.printString_6x8("нажатию кнопки S      ", 0, 3);
  LD.printString_6x8("Текущее положение:    ", 0, 4);
  LD.printString_6x8("                      ", 50, 5);
  LD.printNumber((long)p, 60, 5);
  LD.printString_6x8("градусов", 80, 5);
  // LD.printString_6x8("                      ", 0, 5);
  LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}

void posCalAllPos_LCD() {
  LD.printString_6x8("                      ", 0, 0);
  LD.printString_6x8("                      ", 0, 1);
  LD.printString_6x8("Для перемещения по    ", 0, 2);
  LD.printString_6x8("всем позициям нажми   ", 0, 3);
  LD.printString_6x8("кнопку s. Для выбора  ", 0, 4);
  LD.printString_6x8("позиции нажми кнопку L", 0, 5);
  LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}

void pumpCal_LCD() {
  LD.printString_6x8("Изменяй время налива  ", 0, 0);
  LD.printString_6x8("так чтобы факт. объем ", 0, 1);
  LD.printString_6x8("был равен", 0, 2);
  LD.printString_6x8("мл.", 90, 2);
  LD.printNumber((long)volGlassMax, 70, 2);

  LD.printString_6x8("текущее время налива", 0, 3);
  LD.printString_6x8("              ", 0, 4);
  LD.printNumber((long)pumpMax, 35, 4);
  LD.printString_6x8("мСек", 70, 4);
  LD.printString_6x8("налить нажатием на S", 0, 5);
  LD.printString_6x8("                    ", 0, 6);
  LD.printString_6x8("                    ", 0, 7);
}

void pumpCalExit_LCD() {
  LD.printString_6x8("Нажимай кнопки:       ", 5, 0);
  LD.printString_6x8("set чтобы сохранить   ", 0, 1);
  LD.printString_6x8("L чтобы продолжить    ", 0, 2);
  LD.printString_6x8("R чтобы выйти без     ", 0, 3);
  LD.printString_6x8("сохранения            ", 0, 4);
  LD.printString_6x8("                      ", 0, 5);
  LD.printNumber((long)pumpMax, 10, 5);
  LD.printString_6x8("текущая настройка", 10, 6);
  // LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}

void posCalExit_LCD() {
  LD.printString_6x8("Нажимай кнопки:       ", 5, 0);
  LD.printString_6x8("set чтобы сохранить   ", 0, 1);
  LD.printString_6x8("L чтобы продолжить    ", 0, 2);
  LD.printString_6x8("R чтобы выйти без     ", 0, 3);
  LD.printString_6x8("сохранения            ", 0, 4);
  LD.printString_6x8("                      ", 0, 5);
  LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}

void resStat_LCD() {
  LD.printString_6x8("                      ", 0, 0);
  LD.printString_6x8("Сбросить статистику?  ", 0, 1);
  LD.printString_6x8("Нажимай кнопки:       ", 0, 2);
  LD.printString_6x8("set чтобы сбросить    ", 0, 3);
  LD.printString_6x8("L чтобы выйти         ", 0, 4);
  LD.printString_6x8("                      ", 0, 5);
  LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}

//метод для отображения статистики на экране
void statLCD() {
  res_LCD();
  LD.printString_12x16("Статистика", 0, 0);
  LD.printString_6x8("Всего было налито   ", 10, 3);
  LD.printString_6x8("рюмок", 25, 4);
  LD.printNumber((long)countGlass, 5, 4);
  LD.printNumber((long)totalVol, 5, 5);
  LD.printString_6x8("мл.", 40, 5);
  LD.printNumber((long)currentVol, 5, 6);
  LD.printString_6x8("мл.", 35, 6);
  LD.printString_6x8("за эту пьянку", 50, 6);

  delay(8000);
}


//Метод который отрисовывает вход в меню
void menu_LCD() {
  res_LCD();
  LD.SetInverseText();
  LD.printString_12x16("          ", 0, 0);
  LD.printString_12x16("   MENU   ", 0, 2);
  LD.printString_12x16("          ", 0, 4);
  LD.printString_12x16("          ", 0, 6);
  LD.SetNormalText();
  delay(2000);
  res_LCD();
}

//Метод который отрисовывает вход в меню
void exit_LCD() {
  res_LCD();
  LD.SetInverseText();
  LD.printString_12x16("          ", 0, 0);
  LD.printString_12x16("   EXIT   ", 0, 2);
  LD.printString_12x16("          ", 0, 4);
  LD.printString_12x16("          ", 0, 6);
  LD.SetNormalText();
  delay(2000);
  res_LCD();
}


void volSetupLCD() {
  int temp = 0;
  LD.printString_6x8("   Выберите объем   ", 10, 2);
  LD.printString_6x8("       напитка      ", 10, 3);
  LD.printString_6x8("                    ", 10, 4);
  LD.printString_6x8("мл.", 70, 4);
  LD.printNumber((long)vol, 50, 4);

  temp = vol * 100 / volGlassMax;

  if (temp <= 20)                    LD.printString_6x8("    Что так мало?    ", 5, 5);
  else if (temp <= 40 && temp > 20)  LD.printString_6x8("    Для разгона      ", 5, 5);
  else if (temp <= 60 && temp > 40)  LD.printString_6x8("     Нормально        ", 5, 5);
  else if (temp <= 80 && temp > 60)  LD.printString_6x8(" Не плохо, не плохо   ", 5, 5);
  else if (temp <= 100 && temp > 80) LD.printString_6x8("     Суровый!         ", 5, 5);
}

/*
   Отображение звездочек при наличии рюмок на позициях
*/
void GlassStatLCD(int index) {
  if (index == 0)LD.printString_6x8("*", 10, 7);
  else if (index == 1)LD.printString_6x8("*", 30, 7);
  else if (index == 2)LD.printString_6x8("*", 50, 7);
  else if (index == 3)LD.printString_6x8("*", 70, 7);
  else if (index == 4)LD.printString_6x8("*", 90, 7);
  else if (index == 5)LD.printString_6x8("                        ", 0, 7);
}

void manPumpLCD(int vol) {
  LD.printString_6x8("Наливаю в ручном      ", 10, 1);
  LD.printString_6x8("режиме                ", 10, 2);
  LD.printString_6x8("для остановки         ", 10, 3);
  LD.printString_6x8("отпусти кнопку        ", 10, 4);
  LD.printString_6x8("Налитый обьем:         ", 10, 5);
  LD.printString_6x8("                      ", 10, 6);
  LD.printString_6x8("мл.", 70, 6);
  LD.printNumber((long)vol, 50, 6);

}

void manPumpErrorLCD() {
  LD.printString_6x8("                       ", 10, 2);
  LD.printString_6x8("Поставь свой тазик     ", 10, 3);
  LD.printString_6x8("иначе все зальем!!     ", 10, 4);
  LD.printString_6x8("                       ", 10, 5);
  LD.printString_6x8("                       ", 10, 6);
  LD.printString_6x8("                       ", 10, 7);
}

void resAllEEPROM_LCD() {
  LD.printString_6x8("                      ", 0, 0);
  LD.printString_6x8("Сбросить всю память   ", 0, 1);
  LD.printString_6x8("контроллера?Нужно будет", 0, 2);
  LD.printString_6x8("все настроить заново! ", 0, 3);

  LD.printString_6x8("set чтобы сбросить    ", 0, 4);
  LD.printString_6x8("L чтобы выйти         ", 0, 5);

  LD.printString_6x8("                      ", 0, 6);
  LD.printString_6x8("                      ", 0, 7);
}
