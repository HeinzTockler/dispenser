/*
  звук при включении:
   011 опять работа
   012 даа
   013 че надо хозяин
   014 ну что еще
   015 выпить хочешь
   016 пивка не желаешь

  звук после налива:
   010 между 1 и 2 перерывчик
  006 губит людей не пиво
  007 это его нога, это моя
  008 я тебя поцелую потом
  009 перебрали

  после нажатия на кнопку налива:
  001 пивка для рывка
  003 с кем поведешься с тем и наберешься
  002 сообразим на 3
  004 жить хорошо
  005 пузо от пива

  Переодически:
  017 ну еще по одной
  018 не дай себе засохнуть
  019 ты меня уважаешь
*/

/*
  Метод для получения случайного числа из заданного диапазона
*/
int randomizer(int LL, int HH) {
  // выводим случайное число из диапазона 10..19
  int randNumber = random(LL, HH);
  //if (debug) Serial.println(randNumber);
  return randNumber;
}

/*
   Метод для воспроизведения случайного трека при включении
*/
void play_intro_sound() {
  int rndTrek = 0;
  rndTrek = randomizer(11, 16);
  myMP3.play(rndTrek);
}

/*
   Метод для воспроизведения случайного трека по нажатию кнопки
*/
void play_press_sound() {
  int rndTrek = 0;
  rndTrek = randomizer(1, 5);
  myMP3.play(rndTrek);
}

/*
   Метод для воспроизведения случайного трека после налива
*/
void play_after_sound() {
  int rndTrek = 0;
  rndTrek = randomizer(6, 10);
  myMP3.play(rndTrek);
}

/*
   Метод для воспроизведения случайного трека через определенные промежутки времени
*/
void play_wait_sound() {
  int rndTrek = 0;
  rndTrek = randomizer(17, 19);
  randomTime = randomizer(200, 600); //новый период воспроизвденеия в сек
  randomTime *= 1000;
  myMP3.play(rndTrek);
  // delay(1000);
}


/*
   Метод для воспроизведения трека после налива рюмки в авто режиме
*/
void play_boil_sound() {
  int rndTrek = 0;
  rndTrek = randomizer(17, 19);
  myMP3.play(rndTrek);
}
