
/*
   Проверка кнопок и формировнаие флагов
*/
void start_check() {

  int weight_loc = 0;
  double start_weight = 0;
  int tmp = 0;

  if (digitalRead(but_one_pin) && digitalRead(but_two_pin)) {
    auto_mode = true;
    play_press_sound();
    if (debug)Serial.println("AUTO MODE");
  } else if (digitalRead(but_one_pin)) {
    but_start = true;
    play_press_sound();
    delay(1000);
  } else if (digitalRead(but_two_pin)) {
    if (debug)Serial.println("+++++++++++++++++++++++++MAN++++++++++++++++++++++++++++++");
    start_weight = measure_weight(0);
    //серву передвинуть
    res_LCD();
    while (digitalRead(but_two_pin)) {
      //проверяем рюмку на нулевой позиции
      weight_loc = measure_weight(0);
      if ((mode_detect == 0 && (weight[0] > (int)glass_weight) && (analogRead(tcr1_pin) > tcr_gain))
          || (mode_detect == 1 && (weight[0] > (int)glass_weight))
          || (mode_detect == 2 && (analogRead(tcr1_pin) > tcr_gain))) {
        pos_check(0);
        tmp = abs(weight_loc - start_weight);
        manPumpLCD(tmp);
        delay(50);
        digitalWrite(pump_pin, HIGH);//вкл насос
      } else {
        digitalWrite(pump_pin, LOW);
        manPumpErrorLCD();
        delay(1000);
      }
    }
    digitalWrite(pump_pin, LOW);
    delay(1000);
    res_LCD();
  }
}

void serv_test() {
  int serv_pos = 0;
  serv_pos = servo.read();
  for (int i = 0; i < 5; i++) {
    pos_check_angle(180);
    delay(50);
    pos_check_angle(90);
    delay(50);
    pos_check_angle(0);
    delay(50);
    pos_check_angle(150);
    delay(50);
    pos_check_angle(30);
    delay(50);
    pos_check_angle(120);
    delay(50);
    pos_check_angle(60);
    delay(50);
    pos_check_angle(0);
  }
}

/*
   Метод для перевода сервы в н еобходимое положение
   попробовать написать метод так чтобы серва двигалась половинами от текущего положения
*/
void pos_check(int next_pos) {
  int serv_pos = 0;
  int pos_array[5] = {pos0, pos1, pos2, pos3, pos4};
  digitalWrite(servCtrl, LOW);
  servo.attach(serv_pin);// подключаем серву
  delay(100);
  serv_pos = servo.read();
  if (serv_pos != pos_array[next_pos]) {
    if (debug)Serial.println("need run");
    if (serv_pos < pos_array[next_pos]) {
      for (serv_pos; serv_pos < pos_array[next_pos]; serv_pos += 1)
      {
        servo.write(serv_pos);
        delay(10);
      }
    }
    if (serv_pos > pos_array[next_pos]) {
      for (serv_pos; serv_pos >= pos_array[next_pos]; serv_pos -= 1)
      {
        servo.write(serv_pos);
        delay(10);
      }
    }
    delay(10);
  }
  servo.detach();// подключаем серву
  digitalWrite(servCtrl, HIGH);
}

void servo_init() {
  digitalWrite(servCtrl, LOW);
  servo.attach(serv_pin);
  servo.write(pos0);
  servo.detach();
  digitalWrite(servCtrl, HIGH);
}

/*
   Метод для перевода сервы в н еобходимое положение по заданному углу
*/
void pos_check_angle(int next_pos) {
  int serv_pos = 0;
  bool flag = false;
  int count = 0;

  digitalWrite(servCtrl, LOW);
  servo.attach(serv_pin);
  delay(50);
  serv_pos = servo.read();
  if (debug)Serial.print("next_pos= ");
  if (debug)Serial.println(next_pos);

  //if (debug)Serial.print("nextPOS= ");
  // if (debug)Serial.println(pos_array[next_pos]);

  if (serv_pos != next_pos) {
    flag = true;
    int tempPos = 0;
    while (flag) {
      count++;
      serv_pos = servo.read();
      if (debug)Serial.print("serv_pos= ");
      if (debug)Serial.println(serv_pos);
      tempPos = abs(next_pos - serv_pos);
      if (debug)Serial.print("tempPos= ");
      if (debug)Serial.println(tempPos);

      if (tempPos >= 10) {
        if (debug)Serial.println(">10");
        if (serv_pos < next_pos)
          servo.write(serv_pos + tempPos / 2);
        else
          servo.write(serv_pos - tempPos / 2);
      } else {
        if (debug)Serial.println("<10");
        if (serv_pos < next_pos) {
          tempPos = serv_pos + 1;
          servo.write(tempPos);
        } else {
          tempPos = serv_pos - 1;
          servo.write(tempPos);
        }
      }
      delay(50);
      tempPos = servo.read();
      if (tempPos == next_pos || count >= 200) flag = false;
    }
  }
  servo.detach();// подключаем серву
  digitalWrite(servCtrl, HIGH);
}
/*
   Метод управления насосом
*/
void pump() //ф-я, включающая насос
{
  int time_pump = 0;
  int temp_p = 0;
  long r = 0;
  if (vol > 0) {

    temp_p = map(vol, 1, volGlassMax, 20, pumpMax);
    if (debug)Serial.print("time_pump= ");
    if (debug)Serial.println(temp_p);
    //добавить корректировку по заряду аккумулятора
    r = abs(temp_p * bat_coef / 100);
    if (debug)Serial.print("r= ");
    if (debug)Serial.println(r);
    time_pump = temp_p + r;
      if (debug)Serial.print("time_pump= ");
    if (debug)Serial.println(time_pump);
    delay(300); //ждем 0.3 секунды
    digitalWrite(pump_pin, HIGH);//вкл насос
    delay(time_pump);
    digitalWrite(pump_pin, LOW);//выкл реле
    //  delay(1000);
  }
}

void pump_weight(int glass) //ф-я, включающая насос
{
  int prev = 0;
  int cur = 0;
  int temp = 0;
  int count = 0; //счетчик количество попыток налива
  bool flag = false;

  // if (vol >= 5) {
  prev = measure_weight(glass);
  if (debug)Serial.print("prev_weight= ");
  if (debug)Serial.println(prev);
  flag = true;
  digitalWrite(pump_pin, HIGH);//вкл насос
  while (flag) {
    delay(50); //ждем 0.1 секунды
    cur = measure_weight(glass);
    if (debug)Serial.print("cur_weight= ");
    if (debug)Serial.println(cur);
    temp = abs(cur - prev);
    if  (temp >= vol) {
      flag = false;
      digitalWrite(pump_pin, LOW);//выкл реле
    }
    count++;//защита от бесконечного цикла
    if (count >= 100 || (cur < (prev - glass_weight))) {
      flag = false;
      digitalWrite(pump_pin, LOW);//выкл реле
    }
  }
  digitalWrite(pump_pin, LOW);//выкл реле
}

/*
   Основной метод, после нажатия на кнопку начинается налив
*/
void start_dis() {

  if (but_start) {
    res_LCD();
    if (debug)Serial.println("=============Start pump==============");
    int count = 0;
    if (vol == 0) {
      stopVolLCD();
      delay(4000);
      but_start = false;
    }
    else if (have_glass()) {
      /////////////////////////////////
      runPumpLCD();
      delay(2000);
      //////////////////////////переводим серву в исходное положение//////////////////
      pos_check(0);
      for (int i = 0; i < 5; i++) {
        //если стакан точно есть то переводим серву
        update_status();
        if (glass_stat[i]) {
          //двигаем серву на нужную точку
          pos_check(i);
          if (mode_doz == 0)pump(); else pump_weight(i);
          count++;
          leds[i] = CRGB::Maroon;
          FastLED.show();
          delay(1000);
        }
      }

      //переводим серву назад
      pos_check(0);
      play_after_sound();
      leds_run();
      // if (debug)Serial.println(vol);
      resPumpLCD(count);

      countGlass += count;
      writeEEPROM(countGlass, count_adr);
      if (debug)Serial.print("vol= ");
      if (debug)Serial.println(vol);
      if (debug)Serial.print("count= ");
      if (debug)Serial.println(count);
      currentVol += (vol * count);
      if (debug)Serial.print("currentVol= ");
      if (debug)Serial.println(currentVol);
      totalVol += currentVol;
      writeEEPROM(totalVol, volume_adr);

      delay(4000);
      but_start = false;
    } else {
      but_start = false;
      stopPumpLCD();
      delay(4000);
    }
  }
}

/*
   Метод реализующий авто режим, когда наливатор льет в каждую новую рюмку
*/
void auto_mod_dis() {
  if (auto_mode) {
    res_LCD();
    //bool s = false;
    bool r = false;
    bool l = false;
    bool dir = true; //направление перебора рюмок
    //  uint16_t weight_prev[5] = {0, 0, 0, 0, 0}; //массив с массой, которая была до отсчета
    int stat[5] = {0, 0, 0, 0, 0}; //массив со статусами рюмок
    int i = 0; //итератор рюмок
    int count = 0; //сколько рюмок налили
    int curVol = 0; //объем за время работы в авт режиме
    /*
      статус по рюмкам:
      0 - пусто
      1 - есть рюмка не налито
      2 - есть рюмка налито
    */
    int temp_mode = 0;

    //запоминаем режим работы датчиков, режим ставим временно по обоим
    temp_mode = mode_detect;
    mode_detect = 0;
    autoMode_LCD();
    delay(1000);
    /*
      измерить массу всех рюмок и запомнить ее
      если есть где то рюмки то пройтись и налить если нет то ждать
      наливаем в имеющуюся рюмку или появившуюся
      налили, измеряем массу и запоминаем
      если масса стала меньше значит рюмку сняли, считаем место пустым
      все заново
    */
    //
    /* for (int i = 0; i < 5; i++) {
       weight_prev[i] = measure_weight(i);
      }
      i = 0;*/
    while (auto_mode) {
      // enc.tick();
      //valkoderCtrl();

      autoMode_LCD();
      update_status();

      //есть рюмка
      if (glass_stat[i]) {
        if (stat[i] == 0) stat[i] = 1; //рюмка есть а до этого не было
        if (stat[i] == 1) stat[i] = 1; //рюмка есть и до этого была, значит еще не налили
        if (stat[i] == 2) stat[i] = 2; //рюмка есть и до этого была, значит налили и ждем
      } else {
        if (stat[i] == 0) stat[i] = 0; //нет и не было
        if (stat[i] == 1) stat[i] = 0; //нет а раньше была, значит убрали и мы не налили ничего не делаем
        if (stat[i] == 2) stat[i] = 0; //рюмки нет а раньше налили, значит ее забрали
      }
      /*
            for (int k = 0; k < 5; k++) {
              if (debug)Serial.println(stat[k]);
            }*/

      //наливаем
      if (stat[i] == 1) {
        //двигаем серву на нужную точку
        pos_check(i);
        // if (vol <= 5) pump(); else pump_weight(i);
        if (mode_doz == 0)pump(); else pump_weight(i);
        stat[i] = 2;
        count++;
        curVol += vol;
        leds[i] = CRGB::Maroon;
        FastLED.show();
        play_boil_sound();
        delay(200);
      }

      r = digitalRead(but_one_pin);
      l = digitalRead(but_two_pin);

      if (r) {
        ++vol;
        if (vol >= volGlassMax) vol = volGlassMax;
      }

      if (l) {
        --vol;
        if (vol <= 0) vol = 0;
      }

      if (l && r) {
        auto_mode = false;
        autoModeExit_LCD(count, curVol);
        delay(2000);
      }

      //dir = true вперед
      i++;

      if (i > 4) i = 0;


      delay(200);
    }
    //возвращаем все взад
    pos_check(0);
    mode_detect = temp_mode;
    play_after_sound();

    countGlass += count;
    writeEEPROM(countGlass, count_adr);
    if (debug)Serial.print("vol= ");
    if (debug)Serial.println(vol);
    if (debug)Serial.print("count= ");
    if (debug)Serial.println(count);
    currentVol += curVol;
    if (debug)Serial.print("currentVol= ");
    if (debug)Serial.println(currentVol);
    totalVol += currentVol;
    writeEEPROM(totalVol, volume_adr);
    res_LCD();
  }
}


/*
   Метод проверки есть ли установленные рюмки на местах
*/
bool have_glass() {
  bool temp = false;
  for (int i = 0; i < 5; i++) {
    if (glass_stat[i]) temp = true;
  }
  return temp;
}

/*
   метод для определения наличия рюмки на позиции по индексу с ИК датчика
   Метод для изменения цвета подсветки в зависимости от наличия рюмок
*/
void update_status() {
  int tcr_pin[5] = {tcr1_pin, tcr2_pin, tcr3_pin, tcr4_pin, tcr5_pin};
  //  int *tcr_array[5] = {tcr1, tcr2, tcr3, tcr4, tcr5};
  // uint16_t weight_units = 0;
  bool tcr = false;
  bool weight_flag = false;
  int ik = 0;
  /*
    byte mode_detect = 0;
     режим света
     0 обычный. детектим рюмку по массе и по ИК датчику
     1 детектим только по массе
     2 детектим только по ИК
  */
  //if (debug)Serial.println("stat");
  GlassStatLCD(5);

  for (int i = 0; i < 5; i++) {
    ik = 0;
    measure_weight(i);
    tcr = false;
    weight_flag = false;
    ik = analogRead(tcr_pin[i]);
    if (ik > tcr_gain) tcr = true;
    // if (debug)Serial.println(ik);
    if ( weight[i] > (int)glass_weight) weight_flag = true;

    if (mode_detect == 0) {
      // if (debug)Serial.println("mode 0");
      if (tcr && weight_flag) {
        glass_stat[i] = true;
        leds[i] = CRGB::Gold;
        GlassStatLCD(i);
        //  if (debug)Serial.println("mode 0 all sensor");
      } else if (tcr || weight_flag) {
        glass_stat[i] = false;
        if (tcr) leds[i] = CRGB::Indigo;
        if (weight_flag) leds[i] = CRGB::Chartreuse;
        //if (debug)Serial.println("mode 0 one sensor");
      } else {
        glass_stat[i] = false;
        leds[i] = CRGB::Pink;
        // if (debug)Serial.println("mode 0 none");
      }
    }

    if (mode_detect == 1) {
      //  if (debug)Serial.println("mode 1");
      if (weight_flag) {
        glass_stat[i] = true;
        leds[i] = CRGB::Gold;
        GlassStatLCD(i);
        // if (debug)Serial.println("mode 1 weight");
      } else {
        glass_stat[i] = false;
        leds[i] = CRGB::Indigo;
        //  if (debug)Serial.println("mode 1 none");
      }
    }

    if (mode_detect == 2) {
      // if (debug)Serial.println("mode 2");
      if (tcr) {
        glass_stat[i] = true;
        leds[i] = CRGB::Gold;
        GlassStatLCD(i);
        //  if (debug)Serial.println("mode 2 IK");
      } else {
        glass_stat[i] = false;
        leds[i] = CRGB::Chartreuse;
        // if (debug)Serial.println("mode 2 none");
      }
    }
  }
  FastLED.show();
}

/*
  Метод для обработки данных с тензодатчиков
  index c 0 до 4
*/
int measure_weight(int index) {
  int weight_units = 0;
  // uint16_t weight_gr;
  HX711 HX711_obj;

  if (index == 0)HX711_obj = HX1;
  else if (index == 1)HX711_obj = HX2;
  else if  (index == 2)HX711_obj = HX3;
  else if  (index == 3)HX711_obj = HX4;
  else if  (index == 4)HX711_obj = HX5;

  // weight_units = (double) HX711_obj.get_value(), 5;
  // if (debug)Serial.print("++++1weight= ");
  // if (debug)Serial.println( weight_units);

  weight_units = (int)HX711_obj.get_units(), 5;
  //if (debug)Serial.print("=====weight= ");
  // if (debug)Serial.println( weight_units);
  if (weight_units <= 0 || weight_units > 2000) weight_units = 0;

  // weight_prev[index] = weight[index];
  weight[index] = weight_units;
  // if (debug)Serial.print(index);
  //  if (debug)Serial.print("weight= ");
  // if (debug)Serial.println( weight_units);
  return (int)weight_units;
}

/*
  Метод для выбора количества напитка, отображается на экране
*/
/*
  void set_volum()// ф-я выбора объема на дисплее
  { /*
    int pot = 0;
    pot = analogRead(pot_pin);
    // if (debug)Serial.println(pot);
    vol = map(pot, 0, 1023, 1, 50);// считываем напряж с потенц 0-1000 и переводим в интервал 1-50 мл
    //volSetupLCD();
  // if (pinAValue) vol++;
  // if (pinBValue) vol--;
  // if (vol <= 0) vol = 0;
  // if (vol >= volGlassMax) vol = volGlassMax;
  //if (debug)Serial.println(vol);
  }
*/
