
//int countGlass = 0; //храним здесь количество налитых рюмок
//int totalVol = 0; // храним здесь суммарный налитый обьем

//метод для проверки параметров записанных в пзу
//если то что лежит в еепром отлшичается от дефолта то обновляем.
void checkEEPROM() {

  int pM = 255;
  int p1 = 255;
  int p2 = 255;
  int p3 = 255;
  int p4 = 255;
  int p0 = 255;
  int cl = 255;
  int tv = 255;
  int vg = 255;
  int vS = 255;
  int mode = 255;
  int gw = 255;
  int doz = 255;

  // EEPROM.begin(50);
  EEPROM.get(pumpMax_adr, pM);
  EEPROM.get(pos0_adr, p0);
  EEPROM.get(pos1_adr, p1);
  EEPROM.get(pos2_adr, p2);
  EEPROM.get(pos3_adr, p3);
  EEPROM.get(pos4_adr, p4);
  EEPROM.get(count_adr, cl);
  EEPROM.get(volume_adr, tv);
  EEPROM.get(volGlass_adr, vg);
  EEPROM.get(volSound_adr, vS);
  EEPROM.get(mode_detect_adr, mode);
  EEPROM.get(glass_weight_adr, gw);
  EEPROM.get(mode_doz_adr, doz);

  if (debug)Serial.print("PumpMax= ");
  if (debug)Serial.println(pM);

  if (debug)Serial.print("pos0= ");
  if (debug)Serial.println(p0);

  if (debug)Serial.print("pos1= ");
  if (debug)Serial.println(p1);

  if (debug)Serial.print("pos2= ");
  if (debug)Serial.println(p2);

  if (debug)Serial.print("pos3= ");
  if (debug)Serial.println(p3);

  if (debug)Serial.print("pos4= ");
  if (debug)Serial.println(p4);

  if (debug)Serial.print("Count= ");
  if (debug)Serial.println(cl);

  if (debug)Serial.print("TotalVolume= ");
  if (debug)Serial.println(tv);

  if (debug)Serial.print("VolGlass= ");
  if (debug)Serial.println(vg);

  if (debug)Serial.print("volSound= ");
  if (debug)Serial.println(vS);

  if (debug)Serial.print("Mode_Detect= ");
  if (debug)Serial.println(mode);

  if (debug)Serial.print("glassWeight= ");
  if (debug)Serial.println(gw);

  if (debug)Serial.print("dozeMode= ");
  if (debug)Serial.println(doz);

  if (debug)Serial.println("end EEPROM");

  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (pM != pumpMax && pM != 0 && pM != 255 && pM > 0)pumpMax = pM;
  if (p1 != pos1 && p1 != 0 && p1 != 255 && p1 > 0)pos1 = p1;
  if (p2 != pos2 && p2 != 0 && p2 != 255 && p2 > 0)pos2 = p2;
  if (p3 != pos3 && p3 != 0 && p3 != 255 && p3 > 0)pos3 = p3;
  if (p4 != pos4 && p4 != 0 && p4 != 255 && p4 > 0)pos4 = p4;
  if (p0 != pos0 && p0 != 0 && p0 != 255 && p0 > 0)pos0 = p0;

  if (cl != countGlass && cl != 0 && cl != 255 && cl > 0)countGlass = cl;
  if (tv != totalVol && tv != 0 && tv != 255 && tv > 0)totalVol = tv;
  if (vg != volGlassMax && vg != 0 && vg != 255 && vg > 0)volGlassMax = vg;
  if (vS != volSound && vS != 0 && vS != 255 && vS > 0)volSound = vS;
  if (mode != mode_detect && mode != 0 && mode != 255 && mode > 0)mode_detect = mode;
  if (gw != glass_weight && gw != 0 && gw != 255 && gw > 0)glass_weight = gw;
  if (doz != mode_doz && doz != 0 && doz != 255 && doz > 0)mode_doz = doz;
  EEPROM.end();
}

bool writeEEPROM(int val, byte adr) {
  int param = 0;
  if (debug) Serial.print("adr= ");
  if (debug) Serial.print(adr);
  if (debug) Serial.println(" ");
  // EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.print("param= ");
  if (debug) Serial.print(param);
  if (debug) Serial.println(" ");
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    // EEPROM.commit();
    return true;
  } else return false;
  // EEPROM.end();
}

bool writeEEPROM_str(String val, byte adr) {
  String param ;
  // EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.println(param);
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    // EEPROM.commit();
    return true;
  } else return false;
  // EEPROM.end();
}

/*
  //метод для записи статистики в память
  void stateEEPROM(String val, byte adr) {
  int param = 0;
  EEPROM.get(adr, param);
  if (debug) Serial.println(param);
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
  }
  }*/

//метод для инициализации калибровки насосов
void ResStat() {
  res_LCD();

  bool exit_flag = true;
  bool s = false;
  bool l = false;
  resStat_LCD();
  delay(1000);
  while (exit_flag) {

    resStat_LCD();

    s = !digitalRead(but_val);
    l = digitalRead(but_two_pin);

    if (s) {
      countGlass = 0;
      totalVol = 0;
      writeEEPROM(0, volume_adr);
      writeEEPROM(0, count_adr);
      //writeEEPROM(0, volGlass_adr);
      exit_flag = false;
    }

    if (l) {
      exit_flag = false;
      delay(1000);
    }
    delay(200);
  }
  res_LCD();
}

void ResAllEEPROM() {
  res_LCD();

  bool exit_flag = true;
  bool s = false;
  bool l = false;
  resAllEEPROM_LCD();
  delay(1000);
  while (exit_flag) {

    resAllEEPROM_LCD();

    s = !digitalRead(but_val);
    l = digitalRead(but_two_pin);

    if (s) {
      for (int i = 0 ; i < EEPROM.length() ; i++) {
        EEPROM.write(i, 0);
      }
      exit_flag = false;
    }

    if (l) {
      exit_flag = false;
      delay(1000);
    }
    delay(200);
  }
  res_LCD();

}
