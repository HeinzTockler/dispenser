/*
   Делитель настроен на 3 банки сверху 10ком снизу 6,5ком
  коэф делителя 2.52
   если акб заряжены 12.6 вольт = 5вольт= 1024
   полностью разряжен 2.8в на банку = 8.4 на сборку

   если акб полность разряжен то напруга 8,4 вольт = 3,33 вольта=681
*/
int batMeasure() {
  int analogValue = analogRead (bat_check);
  if (debug) Serial.print("anVal= ");
  if (debug) Serial.println(analogValue);
  int input_voltage = ((analogValue * 4.9) / 1024.0) * 100;
  if (debug) Serial.print("voltage= ");
  if (debug) Serial.println(input_voltage);
  return input_voltage;
}

/*
   Метод для проверки напряжении на батареях
*/
int batCheck() {
  //считать среднее значение
  int voltage = 0;
  int percent = 0;
  voltage = batMeasure();
  batCoefCalc(voltage);

  percent = map(voltage, 333, 490, 0, 100);
  if (percent < 0)percent = 0;
  if (debug) Serial.print("%= ");
  if (debug) Serial.println(percent);
  return percent;
}

/*
   Метод для расчета поправочного коэффициента для поправки на состояние аккумулятора
   при почти полностью высаженных аккумуляторах не доливает порядка 20-23 процентов
*/
void batCoefCalc(int voltage) {

  bat_coef = map(voltage, 333, 460, 28, 0);
  if (bat_coef > 30)bat_coef = 30;
  if (bat_coef < 0)bat_coef = 0;
  if (debug) Serial.print("bat_coef= ");
  if (debug) Serial.println(bat_coef);
}
