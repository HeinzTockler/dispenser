//volatile long pause    = 75;  // Пауза для борьбы с дребезгом
//volatile long lastTurn = 0;   // Переменная для хранения времени последнего изменения

/*
  void val_A()
  {
  if (debug)Serial.println("valA");
  if (micros() - lastTurn < pause) return;  // Если с момента последнего изменения состояния не прошло
  // достаточно времени - выходим из прерывания
  pinAValue = digitalRead(pinA);            // Получаем состояние пинов A и B
  // pinBValue = digitalRead(pinB);
  cli();    // Запрещаем обработку прерываний, чтобы не отвлекаться
  if (!menu && !pump_cal && !pos_cal) {
    if (pinAValue) {
      --vol;
      if (vol <= 0) vol = 0;
    }
  }

  if (pump_cal) {
    if (pinAValue) {
      pumpMax -= 25;
      if (pumpMax < 25) pumpMax = 25;
    }
  }


    if (pos_cal) {
       if (pinAValue) {
         pumpMax -= 1;
         if (pumpMax < 0) pumpMax = 0;
        }
  //}
  sei(); // Разрешаем обработку прерываний
  }*/

/*
  void val_B()
  {
  if (debug)Serial.println("valB");
  if (micros() - lastTurn < pause) return;
  // pinAValue = digitalRead(pinA);
  pinBValue = digitalRead(pinB);

  cli();
  if (!menu && !pump_cal && !pos_cal) {
    if (pinBValue) {
      ++vol;
      if (vol >= volGlassMax) vol = volGlassMax;
    }
  }

  if (pump_cal) {
    pumpMax += 25;
    if (pumpMax >= 10000) pumpMax = 10000;
  }


    if (pos_cal) {
       if (pinAValue) {
         pumpMax += 1;
         if (pumpMax > 180) pumpMax = 180;
        }
  // }

  sei();
  }*/

void valkoderCtrl() {
  // if (debug)Serial.println("valc");
  if (!menu && !pump_cal && !pos_cal) {
    // if (debug)Serial.println("vol_inc");
    if (enc.isRight()) {
      ++vol;
      if (debug)Serial.println("vol_inc");
      if (vol >= volGlassMax) vol = volGlassMax;
    }
    if (enc.isLeft()) {
      if (debug)Serial.println("vol_dec");
      --vol;
      if (vol <= 0) vol = 0;
    }
  }

  if (pump_cal) {
    //  if (debug)Serial.println("pump cal");

    if (enc.isRight()) {
      if (debug)Serial.println("R OC");
      pumpMax += 25;
      if (pumpMax >= 10000) pumpMax = 10000;
    }

    if (enc.isLeft()) {
      if (debug)Serial.println("L OC");
      pumpMax -= 25;
      if (pumpMax < 25) pumpMax = 25;
    }

  }

  /*
    if (pos_cal) {

    }*/

  if (menu) {
    if (enc.isRight()) {
      // if (debug)Serial.println("R");
      ms.next();
      ms.display();
    }
    if (enc.isLeft()) {

      //if (debug)Serial.println("L");
      ms.prev();
      ms.display();
    }
    if (!digitalRead(but_val)) {
      ms.select();
      ms.display();
      delay(500);
    }
  }
}


void lisr() {
  enc.tick();  // отработка в прерывании
  valkoderCtrl();
}
