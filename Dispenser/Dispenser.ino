/*
   Наливатор.Автор Heinz Tockler
   heinztockler@gmail.com
   В проекте используется: насос, сервопривод s3003, одиночные светодиоды WS2812B, тензодатчики HX711 и ИК дальномеры TCRT5000.
   управление сервомотором через отдельный драйвер и управление 0.

  отладить:
  проверить на севших аккумах как работает
  добавить в налив корректировку от напряжения

  настраиваемые переменные:
  pumpMax - время в млсекундах, сколько включать насос чтобы налить максимальное количество жидкости
  pos0-pos4 - позиции сервомотора над рюмками
  volGlassMax - максимальный объем наливаемой жидкости
  glass_weight -масса рюмки, влияет на определение рюмки датчиком массы
  volSound - громкость звука
  mode_detect - режим определения рюмки на позиции 0 - по всем 1 по массе 2 по ИК
  mode_doz - режим дозирования. по массе или по времени
*/
#include <Wire.h>
#include "ASOLED.h"
#include <Servo.h>
#include <FastLED.h>
#include "HX711.h"
#include <EEPROM.h>
#include <MenuSystem.h>
#include <DFPlayerMini_Fast.h>
#include <SoftwareSerial.h>
#include "GyverEncoder.h"

//////Входа///////////////////////////////////////////////////////////////////
#define pump_pin 2 //насос
#define but_one_pin 4 //кнопка
#define but_two_pin 3 //кнопка
#define pot_pin A1 //потенциометр
#define but_val 8 //кнопка валкодера
#define tcr1_pin A0
#define tcr2_pin A2
#define tcr3_pin A3
#define tcr4_pin A10
#define tcr5_pin A8
#define bat_check A9

#define led_pin 5 //светодиоды
#define serv_pin 9 //серва
#define servCtrl 23 //пин питания для сервомотора
#define randomA A15

// HX711 circuit wiring
#define HX_DOUT_1 6
#define HX_SCK_1  7

#define HX_DOUT_2  10
#define HX_SCK_2  11

#define HX_DOUT_3  12
#define HX_SCK_3  13

#define HX_DOUT_4  14
#define HX_SCK_4  15

#define HX_DOUT_5  16
#define HX_SCK_5  17

#define pinA 18 // Пины прерываний для валкодера
#define pinB 19 // Пины прерываний для валкодера

///////Переменные/////////////////////////////////////////////////////////////
int vol = 30; //обьем наливаемого напитка
unsigned long curTime = 0; //для организации циклов прерывания основная работа
#define cicle_update  500 //длительность основной цикл 
unsigned long checkTime = 0; //для организации циклов прерывания проверки батареи
#define check_update  10000 //длительность проверки батареи
unsigned long butPressT = 0;
unsigned long waitedTime = 0; //для организации циклов прерывания по ожиданию
unsigned long randomTime = 300000; //длительность циклов ожидания по умолчанию 5 минут

bool debug = false; //флаг отладки
bool but_start = 0; // флаг о старте налива
bool auto_mode = 0; // флаг о старте налива в авт режиме
bool butLong = false; // флаг длительного нажатия
bool menu = false; // флаг входа в меню
bool pos_cal = false; // флаг режима калибровки позиции сервомотора
bool pump_cal = false; // флаг режима калибровки насоса

//int serv_pos = 0; // Переменная,в которой хранится позиция сервы
//int time_pump = 1000;      // объем для налива в микросек (не более 40 мл=2920 при испыт. напряжен) нужно подбирать при калибровке
int bat_vol = 0; //емкость батареи
int pumpMax = 1750; //переменная равная кол-ву миллисекунд, необходимых для налива 50мл
int volGlassMax = 50; //переменная макс объема наливаемой жидкости
int glass_weight = 25;//порог срабатывания тензодатчика 25 граммб рюмка = 25 грамм
int volSound = 10; //громкость звуков
int mode_detect = 0; //режим опрделения рюмки
int mode_doz = 0; // режим дозирования напитка
int bat_coef = 1; //поправочный коэффициент для поправки на состояние аккумулятора
/*
   режим света
   0 обычный. детектим рюмку по массе и по ИК датчику
   1 детектим только по массе
   2 детектим только по ИК
*/

//volatile int encCounter;
//volatile boolean state0, lastState, turnFlag;

bool glass_stat[5] = {0, 0, 0, 0, 0}; //массив в котором хранятся статус наличия рюмки
int weight[5] = {0, 0, 0, 0, 0}; //массив с текущей массой

double countGlass = 0; //храним здесь количество налитых рюмок
double totalVol = 0; // храним здесь суммарный налитый обьем
int currentVol = 0; // храним здесь суммарный налитый обьем за одну гулянку

#define menudelay  3 //время удержание кнопки сек
#define LED_COUNT 5// Указываем, какое количество пикселей у нашей ленты.
#define BRIGHTNESS 30 //яркость ленты 0-255

uint16_t pos0 = 21 ;//Позиция парковки , здесь нужно подбирать значения в зависимости от шага расположения рюмок
uint16_t pos1 = 47; //Позиция 1 стакана
uint16_t pos2 = 75; //Позиция 2 стакана
uint16_t pos3 = 100; //Позиция 3 стакана
uint16_t pos4 = 125 ;//Позиция 4 стакана

#define tcr_gain 500//порог ИК датчика 1 //здесь настраивается порог сработки датчиков под рюмками
/*#define tcr2 600//порог ИК датчика 2 //при отладке смотрим как меняются значения с датчика
  #define tcr3 600//порог ИК датчика 3 //когда рюмка установлена и когда снята
  #define tcr4 600//порог ИК датчика 4
  #define tcr5 600//порог ИК датчика 5
*/

//////Экземпляры////////////////////////////////////////////////////////////////
CRGB leds[LED_COUNT];//подключаем ленту
//OLED  myOLED(SDA, SCL, 8);// подключаем дисплей
Servo servo;
HX711 HX1;
HX711 HX2;
HX711 HX3;
HX711 HX4;
HX711 HX5;

SoftwareSerial mySerial(31, 30); // RX, TX
DFPlayerMini_Fast myMP3;
Encoder enc(pinA, pinB, but_val, TYPE2);  // для работы c кнопкой и сразу выбираем тип

//адреса хранения данных
#define count_adr 0
#define volume_adr (count_adr+sizeof(double))
#define pumpMax_adr (volume_adr+sizeof(double))
#define pos1_adr (pumpMax_adr+sizeof(int))
#define pos2_adr (pos1_adr+sizeof(int))
#define pos3_adr (pos2_adr+sizeof(int))
#define pos4_adr (pos3_adr+sizeof(int))
#define pos0_adr (pos4_adr+sizeof(int))
#define volGlass_adr (pos0_adr+sizeof(int))
#define volSound_adr (volGlass_adr+sizeof(int))
#define mode_detect_adr (volSound_adr+sizeof(int))
#define glass_weight_adr (mode_detect_adr+sizeof(int))
#define mode_doz_adr (glass_weight_adr+sizeof(int))

void setup()
{
  if (debug)Serial.println("=========start=============");
  Serial.begin(115200);
  LD.init();
  LD.clearDisplay();

  pinMode(pot_pin, INPUT);//пин потенциометра
  pinMode(but_one_pin , INPUT); // кнопка 1
  pinMode(but_two_pin , INPUT); // кнопка 2
  //  pinMode(but_val, INPUT); // кнопка валкдера
  pinMode(pump_pin, OUTPUT);
  digitalWrite(pump_pin, LOW);
  pinMode(servCtrl, OUTPUT);
  digitalWrite(servCtrl, HIGH);
  pinMode(bat_check, INPUT); //висит делитель 1/10кОм  на батареи
  pinMode(randomA, INPUT); //рандомизатор на этом пине
  pinMode(serv_pin, OUTPUT);

  pinMode(tcr1_pin, INPUT);
  pinMode(tcr2_pin, INPUT);
  pinMode(tcr3_pin, INPUT);
  pinMode(tcr4_pin, INPUT);
  pinMode(tcr5_pin, INPUT);

  FastLED.addLeds<WS2812B, led_pin, GRB>(leds, LED_COUNT); //(тип,пин(лента,кол пикселей)
  FastLED.setBrightness( BRIGHTNESS );//устанавливаем яркость ленты

  randomSeed(analogRead(randomA)); //настраиваем генератор случайных чисел
  //if (debug)Serial.println("randomize");
  start_LCD();
  //if (debug)Serial.println("lsd setup");
  checkEEPROM();
  //if (debug)Serial.println("eeeprom setup");
  mySerial.begin(9600);
  myMP3.begin(mySerial);
  myMP3.volume(volSound);
  // Serial.println("Setting volume");
  play_intro_sound();
  leds_pallete(5);
  ResLED();
  // if (debug)Serial.println("sound setup");

  servo_init();

  HX1.begin(HX_DOUT_1, HX_SCK_1);
  HX1.set_scale(883.79);
  HX1.tare();

  HX2.begin(HX_DOUT_2, HX_SCK_2);
  HX2.set_scale(865.31);
  HX2.tare();

  HX3.begin(HX_DOUT_3, HX_SCK_3);
  HX3.set_scale(870.18);
  HX3.tare();

  HX4.begin(HX_DOUT_4, HX_SCK_4);
  HX4.set_scale(854.67);
  HX4.tare();

  HX5.begin(HX_DOUT_5, HX_SCK_5);
  HX5.set_scale(874.70);
  HX5.tare();

  pos_cal = false;
  pump_cal = false;
  menu = false;

  res_LCD();
  bat_vol = batCheck();
  bat_LCD();

  currentVol = 0; //сбрасываем значение выпитого объема за одну гулянку

  curTime = millis();
  checkTime = millis();
  waitedTime = millis();
  
  if (debug)Serial.println("=============end================");
}

void loop()
{

  enc.tick();
  valkoderCtrl();

  if (!menu) {
    if (millis() > (curTime + cicle_update)) {
      //if (debug)Serial.println("up");
      curTime = millis();
      //обрабатываем сигнал с пер резистора и отоюражаем на экране
      //  set_volum();
      volSetupLCD();
      //устанавливаем цвет светодиодов и опрашиваем ИК датчики
      update_status();
      //Проверяем нажата ли какая нибудь кнопка
      start_check();
      //Начинаем процедуру налива
      start_dis();
      auto_mod_dis();
    } else if (curTime > millis()) {
      curTime = millis();
      Serial.println("Overflov cicle_update");
    }

    if (millis() > (checkTime + check_update)) {
      checkTime = millis();
      if (debug)Serial.println("bat");
      bat_vol = batCheck();
      bat_LCD();
    } else if (checkTime > millis()) {
      checkTime = millis();
      Serial.println("Overflov cicle_update");
    }

    if (millis() > (waitedTime + randomTime)) {
      waitedTime = millis();
      play_wait_sound();
      leds_pallete(3);
    } else if (waitedTime > millis()) {
      waitedTime = millis();
      Serial.println("Overflov cicle_update");
    }

    if (!digitalRead(but_val)) {
      if (!butLong) {
        butPressT = millis();
        butLong = true;
        if (debug)Serial.println("setup");
      } else if (millis() - butPressT > (menudelay * 1000)&butPressT != 0) {
        if (debug)Serial.println("long");
        butLong = false;
        butPressT = 0;
        menu = true;
        menu_LCD();
        menuInit();
        ResLED();
      }
    } else butLong = false;

  } else {
    buton_handler();
  }
}
