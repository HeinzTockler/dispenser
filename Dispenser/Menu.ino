class MyRenderer : public MenuComponentRenderer {
  public:
    void render(Menu const& menu) const {
      menu.render(*this);
      menu.get_current_component()->render(*this);
    }

    void render_menu_item(MenuItem const& menu_item) const {
      res_LCD();
      // LD.printString_6x8("                   ", 0, 0);
      LD.printString_6x8(menu_item.get_name(), 0, 3);
    }

    void render_back_menu_item(BackMenuItem const& menu_item) const {
      res_LCD();
      LD.printString_12x16("          ", 0, 0);
      LD.printString_12x16(menu_item.get_name(), 10, 4);
    }

    void render_numeric_menu_item(NumericMenuItem const& menu_item) const {
      res_LCD();
      char buf[10];
      menu_item.get_formatted_value().toCharArray(buf, 10);
      // если пишется дефолтное значение, то попробовать устанавливать его при выборе меню
      LD.printString_6x8("                    ", 10, 2);
      LD.printString_6x8(menu_item.get_name(), 10, 2);
      LD.printString_6x8("                    ", 10, 4);
      if (menu_item.get_name() == "Опред. рюмки") {
        if (menu_item.get_value() == 0) {
          LD.printString_6x8(" Масса + ИК сенсор ", 10, 4);
        } else if (menu_item.get_value() == 1) {
          LD.printString_6x8("    Только Масса    ", 10, 4);
        } else  if (menu_item.get_value() == 2) {
          LD.printString_6x8("  Только ИК сенсор  ", 10, 4);
        }
      } else if (menu_item.get_name() == "Режим дозирования") {
        if (menu_item.get_value() == 0) {
          LD.printString_6x8("     по времени     ", 10, 4);
        } else if (menu_item.get_value() == 1) {
          LD.printString_6x8("  по массе напитка  ", 10, 4);
        }
      } else LD.printString_6x8(buf, 10, 4);
    }

    void render_menu(Menu const& menu) const {
      res_LCD();
      LD.printString_12x16("          ", 0, 0);
      LD.printString_12x16(menu.get_name(), 0, 2);
    }
};
MyRenderer my_renderer;

/*
  настраиваемые переменные:
  volGlassMax - максимальный объем наливаемой жидкости


  pumpMax - время в млсекундах, сколько включать насос чтобы налить максимальное количество жидкости
  pos0 - pos4 - позиции сервомотора над рюмками
*/

MenuSystem ms(my_renderer);
NumericMenuItem volMax("Макс. объем", &volGlassMax_sel, volGlassMax, 10, 100, 1, nullptr);
NumericMenuItem glassW("Масса рюмки", &glassWeight_sel, glass_weight, 5, 200, 1, nullptr);
MenuItem muStat("Статистика", &stat_sel);
MenuItem muPumpCal("Калибровка насоса", &pumpCal_sel);
MenuItem muServCal("Калибровка сервы", &servCal_sel);
MenuItem muResStat("Сброс статистики", &stat_res);
NumericMenuItem muSound("Громкость звука", &vol_Sound, volSound , 0, 30, 1, nullptr);
NumericMenuItem muMode("Опред. рюмки", &mode_detect_sel, mode_detect , 0, 2, 1, nullptr);
NumericMenuItem muDoz("Режим дозирования", &mode_doz_sel, mode_doz , 0, 1, 1, nullptr);
MenuItem muROMClear("Очистка памяти", &ROMClear_sel);

void volGlassMax_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.print("volMax= ");
  if (debug)Serial.println(volMax.get_value());
  volGlassMax = volMax.get_value();
  writeEEPROM(volGlassMax, volGlass_adr);
}

void glassWeight_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.print("glass_weight= ");
  if (debug)Serial.println(glassW.get_value());
  glass_weight = glassW.get_value();
  writeEEPROM(glass_weight, glass_weight_adr);
}

void stat_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.println("stat LCD");
  statLCD();
  ms.display();
}

void stat_res(MenuComponent * p_menu_component) {
  if (debug)Serial.print("Reset_Stat");
  ResStat();
  ms.display();
}

void pumpCal_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.println("cal Pump");
  pump_cal = true;
  pump_Cal();
  ms.display();
}

void servCal_sel(MenuComponent * p_menu_component) {
  if (debug)Serial.println("cal serv");
  // statLCD();
  pos_cal = true;
  servo_Cal();
  ms.display();
}

void vol_Sound(MenuComponent * p_menu_component) {
  volSound = muSound.get_value();
  if (debug)Serial.print("volSound= ");
  if (debug)Serial.println(volSound);
  myMP3.volume(volSound);
  play_intro_sound();
  writeEEPROM(volSound, volSound_adr);
}

void mode_detect_sel(MenuComponent * p_menu_component) {
  mode_detect = muMode.get_value();
  writeEEPROM(mode_detect, mode_detect_adr);
}

void mode_doz_sel(MenuComponent * p_menu_component) {
  mode_doz = muDoz.get_value();
  writeEEPROM(mode_doz, mode_doz_adr );
}

void ROMClear_sel(MenuComponent * p_menu_component) {
  ResAllEEPROM();
  ms.display();
}

void menuInit() {
  ms.get_root_menu().add_item(&volMax);
  ms.get_root_menu().add_item(&glassW);
  ms.get_root_menu().add_item(&muSound);
  ms.get_root_menu().add_item(&muMode);
  ms.get_root_menu().add_item(&muDoz);
  ms.get_root_menu().add_item(&muPumpCal);
  ms.get_root_menu().add_item(&muServCal);

  ms.get_root_menu().add_item(&muStat);
  ms.get_root_menu().add_item(&muResStat);
  ms.get_root_menu().add_item(&muROMClear);
  ms.display();
}

void buton_handler() {
  // if (digitalRead(but_val)) Serial.println("val_but_press");

  //bool s = digitalRead(but_val);
  bool r = digitalRead(but_one_pin);
  bool l = digitalRead(but_two_pin);

  if (l && r) {
    menu = false;//выход из меню
    pos_cal = false;
    pump_cal = false;
    exit_LCD();

  }
  /*
    else {
    if (l) {
      ms.prev();
      ms.display();
    }
    if (r) {
      ms.next();
      ms.display();
    }
    if (s) {
      ms.select();
      //if (debug)Serial.println(ms.get_current_menu());
      ms.display();
    }
    }*/
  //delay(50);
}
