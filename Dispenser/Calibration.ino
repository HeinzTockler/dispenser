//метод для инициализации калибровки насосов
void pump_Cal() {
  res_LCD();

  int temp_max = 0;
  temp_max = pumpMax;
  bool exit_flag = false;
   bool s = false;
  bool r = false;
  bool l = false;
  pumpCal_LCD();
  delay(1000);
  while (pump_cal) {

    // enc.tick();
    //valkoderCtrl();

    pumpCal_LCD();

    s = !digitalRead(but_val);
    r = digitalRead(but_one_pin);
    l = digitalRead(but_two_pin);

    if (s) {
      vol = volGlassMax;
      pump();
    }
    
     if (r) {
      //if (debug)Serial.println("R OC");
      pumpMax += 25;
      if (pumpMax >= 10000) pumpMax = 10000;
    }

    if (l) {
      //if (debug)Serial.println("L OC");
      pumpMax -= 25;
      if (pumpMax < 25) pumpMax = 25;
    }

    if (l && r) {
      exit_flag = true;
      res_LCD();
      delay(1000);
      while (exit_flag) {
        pumpCalExit_LCD();
        //s = digitalRead(but_val);
        r = digitalRead(but_one_pin);
        l = digitalRead(but_two_pin);
        if (!digitalRead(but_val)) {
          writeEEPROM(pumpMax, pumpMax_adr);
          pump_cal = false;//выход из меню
          exit_flag = false;
        }
        if (l)exit_flag = false;
        if (r) {
          pumpMax = temp_max;
          exit_flag = false;
          pump_cal = false;
        }
        delay(200);
      }
    }
     delay(200);
  }
  pump_cal = false;
}

//метод для инициализации калибровки позиций сервомотора
void servo_Cal() {
  res_LCD();
  int pos[] = {pos0, pos1, pos2, pos3, pos4};
  int pos_temp[] = {0, 0, 0, 0, 0};
  int index = 1;

  bool exit_flag = false;
  bool pos_selected = false;

  bool s = false;
  bool r = false;
  bool l = false;

  //переписываем все значения позиций во временное хранилище
  for (int i = 0; i < 5; i++) {
    if (debug)Serial.println(pos[i]);
    pos_temp[i] = pos[i];
    if (debug)Serial.println(pos_temp[i]);
  }

  posCal_LCD(index);
  delay(1000);

  while (pos_cal) {
    //enc.tick();
    //valkoderCtrl();
    //delay(200);
    s = !digitalRead(but_val);
    r = digitalRead(but_one_pin);
    l = digitalRead(but_two_pin);
    // if (debug)Serial.print("index= ");
    //  if (debug)Serial.println(index);


    if (index != 6) {
      if (!pos_selected) posCal_LCD(index); else posCalSelect_LCD(pos[index - 1]);
    } else posCalAllPos_LCD();

    if (s) {
      delay(200);
      if (index == 6) {
        //перемещаемся по всем позициям
        for (int i = 0; i < 5; i++) {
          pos_check_angle(pos[i]);
          delay(200);
        }
      } else if (!pos_selected) {
        pos_selected = true;
        //перемещение сервы на эту позицию первоначальное
        //  pos_check_angle(pos[index - 1]);
        // pos_check(i);
      } else {
        pos_selected = false;
      }
    }

    if (l) {
      if (!pos_selected) {
        index -= 1;
        if (index <= 0) index = 1;
        pos_check_angle(pos[index - 1]);
      } else {
        pos[index - 1] -= 1;
        if ( pos[index - 1] <= 0)pos[index - 1] = 0;
        //перемещение сервы на эту позицию
        pos_check_angle(pos[index - 1]);
      }
      delay(200);
    }

    if (r) {
      if (!pos_selected) {
        index += 1;
        if (index > 6) index = 6;
        // pos_check(i);
      } else {
        pos[index - 1] += 1;
        if ( pos[index - 1] > 180)pos[index - 1] = 180;
        //перемещение сервы на эту позицию
        pos_check_angle(pos[index - 1]);
      }
      delay(200);
    }

    if (l && r) {
      exit_flag = true;
      res_LCD();
      delay(1000);
      while (exit_flag) {
        posCalExit_LCD();
        s = !digitalRead(but_val);
        r = digitalRead(but_one_pin);
        l = digitalRead(but_two_pin);

        if (s) {
          pos0 = pos[0];
          pos1 = pos[1];
          pos2 = pos[2];
          pos3 = pos[3];
          pos4 = pos[4];
          writeEEPROM(pos0, pos0_adr);
          writeEEPROM(pos1, pos1_adr);
          writeEEPROM(pos2, pos2_adr);
          writeEEPROM(pos3, pos3_adr);
          writeEEPROM(pos4, pos4_adr);

          pos_cal = false;//выход из меню
          exit_flag = false;
        }
        if (l)exit_flag = false;
        if (r) {
          for (int i = 0; i < 5; i++) {
            //    if (debug)Serial.println(pos[i]);
            pos[i] = pos_temp[i];
            //  if (debug)Serial.println(*pos[i]);
          }
          pos_cal = false;
          exit_flag = false;
        }
        delay(200);
      }
    }
  }
  pos_cal = false;
}
